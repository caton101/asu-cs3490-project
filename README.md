# Markdown to HTML Converter

## Description

This is a converter written in Haskell that converts Markdown files into HTML
webpages. It recognizes most [GitHub Flavored Markdown][0] (GFM) features.
Please note there may be parsing errors for braces and dashes. This is a
limitation of the shift-reduce parser.

This is a project for my CS 3490 class at Appalachian State University. It is
the combined effort of [Chase Faine][1] and myself. This repo was taken from our
[original GitHub repository][2] and placed under a license with his permission.

## WARNING TO STUDENTS

This code was made public to demonstrate the work of Chase Faine and I. You may
use this repo to learn from but you are not allowed to copy it. Copying our work
without proper citations is considered plagiarism and may lead to punishment if
caught by your academic institution.

## Directory Structure

Directories:

- `build`: the converter binary and our submission zip file
- `outline`: an outline of all functions in our code when it was written
- `project`: the Haskell code and three test cases
- `proposal`: the project proposal sent to our professor for approval
- `report`: the final report which was bundled in our project submission
- `submission`: a copy of our submission files used by the zip command

**Note**: The `build` and `submission` directories do not exist until after the
`make` command is ran at least once.

Important Files:

- `proposal/proposal-final.pdf`: the proposal submitted to our professor
- `outline/outline-final.pdf`: the outline submitted to our professor
- `report/final-report-final.pdf`: the report submitted to our professor
- `submission-final.zip`: the final submission to our professor
- `project/converter.hs`: the source code for the converter
- `makefile`: the build rules for the converter and project documents
- `.gitignore`: forces git to ignore the built files
- `LICENSE`: a copy of the GNU GPL v3
- `README.md`: a description of this repository

## Dependencies

- [The Glasgow Haskell Compiler][3] (we used version 9.0.1)
- [Pandoc][4]
- [TeX Live][5]

**Note**: `Pandoc` and `TeX Live` are only needed to build the submission PDF
files. These can be ignored if you are only interested in the Haskell code or
wish to use the prebuilt PDF files.

## Building

1. Clone the repo:
`git clone https://gitlab.com/caton101/asu-cs3490-project.git`
2. Enter the directory:
`cd asu-cs3490-project`
3. Run the make command:
`make`

**Note**: If `make` reports all the targets are up to date, you should use the
`-B` flag to force build the project (ex: `make -B`).

Same as above, but easier to copy and paste:

```
git clone https://gitlab.com/caton101/asu-cs3490-project.git
cd asu-cs3490-project
make
```

## Usage

Once built, the converter will be inside the `build` directory. You must run the
converter from a terminal. The command syntax is `converter <infile> <outfile>`.

For example, `./converter myFile.md myFile.html` will run the `converter` from
the current working directory. The `myFile.md` argument is a properly formatted
Markdown file in the current working directory. The `myFile.html` argument is
the output file which will be placed in the current working directory.

You may build the test cases with the following commands:

```
./build/converter project/doc1.md doc1.html
./build/converter project/doc2.md doc2.html
./build/converter project/doc3.md doc3.html
```

## License

This project is licensed under the GNU GPL v3. See the LICENSE file for more
information.

## Contributing

Thanks for your interest, but don't do it. This was only made for a class
project. I have no interest in maintaining this project. I encourage you to
either contribute to another one of my repositories or work on extending this
project yourself.

[0]: https://docs.github.com/en/github/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax
[1]: https://github.com/ChaseFaine
[2]: https://github.com/Caton101/cs3490-project
[3]: https://www.haskell.org/ghc/
[4]: https://pandoc.org/
[5]: https://tug.org/texlive/
